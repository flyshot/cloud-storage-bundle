Installation
-----

Use [Composer](https://getcomposer.org/) to add library to project. 
Add this private repository to your `composer.json` and run:

```bash
composer req flyshot/cloud-storage-bundle
```
Provide parameters in your env file:

     GCP_APPLICATION_CREDENTIALS
     GCP_STORAGE_BUCKET_NAME
     GCP_STORAGE_BASE_FOLDER
    
To test library run in library folder:

```bash
composer install && ./vendor/phpunit/phpunit/phpunit
```
