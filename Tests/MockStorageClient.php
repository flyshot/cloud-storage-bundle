<?php
/**
 * Created by PhpStorm.
 * User: archer
 * Date: 15/02/2019
 * Time: 21:37.
 */

namespace Flyshot\CloudStorageBundle\Tests;

use Flyshot\CloudStorageBundle\StorageClient\PathResolver;
use Flyshot\CloudStorageBundle\StorageClient\StorageClient;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class MockStorageClient extends StorageClient
{
    private const PIXEL = 'iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mP8/x8AAwMCAO+ip1sAAAAASUVORK5CYII=';

    public function __construct()
    {
        // Disable parent constructor
    }

    public function upload(string $objectName, UploadedFile $uploadedFile, int $options = 0): string
    {
        return $objectName;
    }

    public function remove(string $internalPath): void
    {
    }

    public function downloadToFile(string $internalPath, string $destination, ?string $postfix = null)
    {
        file_put_contents($destination, base64_decode(self::PIXEL));
    }

    public function getPublicUrl(string $internalPath, ?string $postfix = null): string
    {
        return PathResolver::GCE_STORAGE_URL;
    }
}
