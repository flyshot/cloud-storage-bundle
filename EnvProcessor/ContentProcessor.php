<?php
/**
 * Created by PhpStorm.
 * User: archer
 * Date: 14/03/2019
 * Time: 21:50
 */

namespace Flyshot\CloudStorageBundle\EnvProcessor;

use Symfony\Component\DependencyInjection\EnvVarProcessorInterface;

class ContentProcessor implements EnvVarProcessorInterface
{
    public function getEnv($prefix, $name, \Closure $getEnv)
    {
        $content = $getEnv($name);
        if (is_file($content)) {
            $content = file_get_contents($content);
        }

        return $content;
    }

    public static function getProvidedTypes()
    {
        return ['content' => 'string'];
    }
}