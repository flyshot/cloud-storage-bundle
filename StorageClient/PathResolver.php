<?php
/**
 * Created by PhpStorm.
 * User: Alexander Samusevich
 * Date: 26/08/2019
 * Time: 10:48
 */

namespace Flyshot\CloudStorageBundle\StorageClient;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class PathResolver
{
    const GCE_STORAGE_URL = 'https://storage.googleapis.com';

    private string $bucketName;
    private string $baseFolder;

    public function __construct(string $bucketName, string $baseFolder)
    {
        $this->bucketName = $bucketName;
        $this->baseFolder = $baseFolder;
    }

    /**
     * @param string $path - Internal or absolute path
     * @param string|null $postfix - prefix for thumbnail url generation
     * @return string
     */
    public function getPublicUrl(string $path, ?string $postfix = null): string
    {
        $internalPath = $this->convertPublicUrlToInternalPath($path);

        // Check if path is absolute
        if ($this->isPathAbsolute($internalPath)) {
            return $internalPath;
        }

        if (null !== $postfix) {
            $internalPath = $this->addPostfix($internalPath, $postfix);
        }

        $urlPrefix = $this->getUrlPrefix();

        return $urlPrefix.$internalPath;
    }

    public function getInternalPath(string $objectName, UploadedFile $uploadedFile, string $postfix = null): string
    {
        $internalPath = $objectName.'.'.$uploadedFile->getClientOriginalExtension();

        if (!empty($this->baseFolder)) {
            $internalPath = $this->baseFolder.'/'.$internalPath;
        }

        if (null !== $postfix) {
            $internalPath = $this->addPostfix($internalPath, $postfix);
        }

        return $internalPath;
    }

    public function addPostfix(string $path, string $postfix)
    {
        return preg_replace("/(\.[^.]+)$/", '.'.$postfix.'$1', $path);
    }

    public function convertPublicUrlToInternalPath(string $publicUrl): string
    {
        $urlPrefix = $this->getUrlPrefix();

        return str_replace($urlPrefix, '', $publicUrl);
    }

    private function isPathAbsolute(string $path): bool
    {
        return preg_match("/^https?\:\/\//", $path);
    }

    private function getUrlPrefix(): string
    {
        return self::GCE_STORAGE_URL.'/'.$this->bucketName.'/';
    }
}
