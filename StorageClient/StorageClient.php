<?php
/**
 * Created by PhpStorm.
 * User: marina
 * Date: 2/6/19
 * Time: 12:17 PM
 */

namespace Flyshot\CloudStorageBundle\StorageClient;

use Flyshot\CloudStorageBundle\StorageClient\Handler\HandlerFactory;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class StorageClient
{
    private $pathResolver;
    private $handlerFactory;

    public function __construct(PathResolver $pathResolver, HandlerFactory $handlerFactory)
    {
        $this->pathResolver = $pathResolver;
        $this->handlerFactory = $handlerFactory;
    }

    /**
     * Upload a file into cloud.
     *
     * @param string $objectName - the name of the object.
     * @param UploadedFile $uploadedFile - uploaded file
     * @param int $options - bit field with options
     *
     * @return string - new filename.
     */
    public function upload(string $objectName, UploadedFile $uploadedFile, int $options = 0): string
    {
        $internalPath = $this->pathResolver->getInternalPath($objectName, $uploadedFile);
        $handler = $this->handlerFactory->create($internalPath);

        $handler->upload($internalPath, $uploadedFile, $options);

        return $internalPath;
    }

    public function remove(string $path): void
    {
        $internalPath = $this->makePathInternal($path);
        $handler = $this->handlerFactory->create($internalPath);
        $handler->remove($internalPath);
    }

    public function getPublicUrl(string $internalPath, ?string $postfix = null): string
    {
        return $this->pathResolver->getPublicUrl($internalPath, $postfix);
    }

    /**
     * Download file from cloud to local filesystem
     * into specific file passed as $destination param
     */
    public function downloadToFile(string $internalPath, string $destination, ?string $postfix = null)
    {
        $internalPath = $this->makePathInternal($internalPath);
        $handler = $this->handlerFactory->create($internalPath);

        return $handler->downloadToFile($internalPath, $destination, $postfix);
    }

    /**
     * Download file from cloud to local filesystem
     * into temparary file and set filename to $destination link
     */
    public function download(string $internalPath, &$destination, ?string $postfix = null)
    {
        // Extract an extension from file name
        preg_match("/\.[0-9a-z]+$/i", $internalPath, $matches);

        // Generate an unique temporary name
        $destination = sprintf(
            '%s/%s.%s',
            sys_get_temp_dir(),
            time().rand(0, 1000),
            $matches[0]);

        return $this->downloadToFile($internalPath, $destination, $postfix);
    }

    private function makePathInternal(string $path): string
    {
        return $this->pathResolver->convertPublicUrlToInternalPath($path);
    }
}
