<?php
/**
 * Created by PhpStorm.
 * User: archer
 * Date: 26/08/2019
 * Time: 11:47
 */

namespace Flyshot\CloudStorageBundle\StorageClient\Handler;

use Gumlet\ImageResize;
use Liip\ImagineBundle\Imagine\Filter\FilterManager;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageHandler extends AbstractHandler
{
    public const DISABLE_PREVIEW = 1;

    private const FORMATS = [
        'thumbnail' => 256,
        'thumbnail-f' => 256,
        'medium' => 768,
        'large' => 1280,
    ];

    public function upload(string $internalPath, UploadedFile $uploadedFile, int $options = 0): void
    {
        $this->initBucket();

        $source = $uploadedFile->getPathname();

        // Upload original
        $this->uploadFile($source, $internalPath);

        // Make and upload thumbs
        if (!$options & self::DISABLE_PREVIEW) {
            foreach (self::FORMATS as $postfix => $format) {

                $image = new ImageResize($source);
                $image->resizeToLongSide($format, true);

                $tempFile = tempnam(sys_get_temp_dir(), $internalPath);
                $image->save($tempFile, IMAGETYPE_JPEG);

                if ('thumbnail-f' === $postfix) {
                    $this->applyFilters($tempFile);
                }

                $path = $this->pathResolver->addPostfix($internalPath, $postfix);
                try {
                    $this->uploadFile($tempFile, $path);
                } finally {
                    unlink($tempFile);
                }
            }
        }
    }

    private function applyFilters(string $path): void
    {
        $image = new \Imagick($path);
        $image->addNoiseImage(\Imagick::NOISE_GAUSSIAN, \Imagick::CHANNEL_DEFAULT);
        $image->writeImage($path);
    }

    private function uploadFile(string $source, string $internalPath)
    {
        $file = fopen($source, 'r');
        if (false === $file) {
            throw new \RuntimeException(sprintf('Cannot open file %s', $source));
        }

        // GC SDK will close the file resource automaticaly
        $object = $this->bucket->upload($file, [
            'name' => $internalPath,
        ]);
        $object->update(['acl' => []], ['predefinedAcl' => 'PUBLICREAD']);
    }

    public function remove(string $internalPath): void
    {
        $this->initBucket();

        $postfixes = array_merge([''], array_keys(self::FORMATS));
        foreach ($postfixes as $postfix) {
            $path = $internalPath;
            if (!empty($postfix)) {
                $path = $this->pathResolver->addPostfix($internalPath, $postfix);
            }

            $object = $this->bucket->object($path);
            if ($object->exists()) {
                $object->delete();
            }
        }
    }

    public function support(string $internalPath): bool
    {
        if (!preg_match("/\.([^\.]+)$/", $internalPath, $matches)) {
            return false;
        }
        $ext = $matches[1];

        return in_array(strtolower($ext), ['jpg', 'jpeg', 'png', 'gif']);
    }

    public function downloadToFile(string $internalPath, string $uploadedPath, ?string $postfix = null)
    {
        $this->initBucket();

        if (null !== $postfix) {
            $internalPath = $this->pathResolver->addPostfix($internalPath, $postfix);
        }

        $object = $this->bucket->object($internalPath);
        if ($object->exists()) {
            $object->downloadToFile($uploadedPath);
            return true;
        }

        throw new \RuntimeException(sprintf('File %s was not downloaded', $internalPath));
    }
}
