<?php
/**
 * Created by PhpStorm.
 * User: archer
 * Date: 26/08/2019
 * Time: 11:42
 */

namespace Flyshot\CloudStorageBundle\StorageClient\Handler;

use Flyshot\CloudStorageBundle\StorageClient\PathResolver;
use Google\Cloud\Storage\Connection\IamBucket;
use Google\Cloud\Storage\StorageClient as CloudStorageClient;
use \Google\Cloud\Storage\Bucket as Bucket;
use Symfony\Component\HttpFoundation\File\UploadedFile;

abstract class AbstractHandler
{
    /**
     * @var Bucket
     */
    protected $bucket;

    private $bucketName;
    private $credentials;
    protected $pathResolver;

    public function __construct(array $credentials, string $bucketName, PathResolver $pathResolver)
    {
        $this->credentials = $credentials;
        $this->bucketName = $bucketName;
        $this->pathResolver = $pathResolver;
    }

    protected function initBucket(): void
    {
        if (!$this->bucket) {
            $storage = new CloudStorageClient([
                'keyFile' => $this->credentials,
            ]);
            $this->bucket = $storage->bucket($this->bucketName);
        }
    }

    abstract public function upload(string $internalPath, UploadedFile $uploadedFile, int $options = 0): void;

    abstract public function remove(string $internalPath): void;

    abstract public function support(string $internalPath): bool;
}
