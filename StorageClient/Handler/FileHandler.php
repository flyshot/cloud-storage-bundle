<?php
/**
 * Created by PhpStorm.
 * User: archer
 * Date: 26/08/2019
 * Time: 11:47
 */

namespace Flyshot\CloudStorageBundle\StorageClient\Handler;

use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileHandler extends AbstractHandler
{
    public function upload(string $internalPath, UploadedFile $uploadedFile, int $options = 0): void
    {
        $this->initBucket();

        $source = $uploadedFile->getPathname();
        $file = fopen($source, 'r');

        $object = $this->bucket->upload($file, [
            'name' => $internalPath,
        ]);

        $object->update(['acl' => []], ['predefinedAcl' => 'PUBLICREAD']);
    }

    public function remove(string $internalPath): void
    {
        $this->initBucket();

        $object = $this->bucket->object($internalPath);
        if ($object->exists()) {
            $object->delete();
        }
    }

    public function support(string $internalPath): bool
    {
        return true;
    }
}
