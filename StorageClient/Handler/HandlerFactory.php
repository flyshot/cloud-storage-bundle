<?php
/**
 * Created by PhpStorm.
 * User: archer
 * Date: 26/08/2019
 * Time: 11:46
 */

namespace Flyshot\CloudStorageBundle\StorageClient\Handler;

use Psr\Log\LoggerInterface;

class HandlerFactory
{
    /**
     * @var iterable|AbstractHandler[]
     */
    private $handlers;
    private $logger;

    public function __construct(iterable $handlers, LoggerInterface $logger)
    {
        $this->handlers = $handlers;
        $this->logger = $logger;
    }

    public function create(string $internalPath): AbstractHandler
    {
        foreach ($this->handlers as $handler) {
            if ($handler->support($internalPath)) {
                $this->logger->info(sprintf('Storage handler created %s', get_class($handler)));
                return $handler;
            }
        }

        throw new \RuntimeException(sprintf('Cannot create storage handler for file with internal path: %s', $internalPath));
    }
}